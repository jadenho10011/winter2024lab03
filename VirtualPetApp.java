import java.util.Scanner;

public class VirtualPetApp{
	public static void main(String[] args){
		
		Scanner reader = new Scanner(System.in);
		
		Fox[] skulk = new Fox[4];
		for (int i = 0; i < skulk.length; i++) {
            skulk[i] = new Fox();
			System.out.println("Enter which subspecie of fox it is:");
			skulk[i].specie = reader.nextLine();
			System.out.println("Enter the Scientific name of the fox:");
			skulk[i].latinName = reader.nextLine();
			System.out.println("Enter around how many hours it stays active during a day:");
			skulk[i].activeHours = Integer.parseInt(reader.nextLine());
        } 
		System.out.println(skulk[3].specie);
		System.out.println(skulk[3].latinName);
		System.out.println(skulk[3].activeHours);

		skulk[0].bePresented();
		skulk[0].createGroup();
	}
}