import java.util.Random;

public class Fox{
    public String specie;
    public String latinName;
    public int activeHours;
	public int groupSize;
	
	public void bePresented(){
		System.out.println("This fox, is a " + specie + " fox that is also known as the " + latinName + ", is active for " + activeHours + " hours per day.");
	}
	
	 public void createGroup() {
        Random random = new Random();
        groupSize = random.nextInt(10) + 1;
        System.out.println("This fox is part of a group of " + groupSize + " foxes.");
    }
}
